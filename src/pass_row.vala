 /* speedticket
    Copyright (C) 2020  Sebastian Spaeth & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Gtk;
namespace Speedticket {


  public class PassRow : Gtk.ListBoxRow {
    /* the underlying Pass */
    public Pass pass;

    /* Constructor opening an empty pass */
    public PassRow(Pass pass) {
      debug("Creating passrow");
      this.pass = pass;

      var box = new Box(Gtk.Orientation.HORIZONTAL, 0);
      var label = new Label(pass.get_description());
      if (pass.icon != null) {
        var icon = new Image.from_pixbuf(pass.icon);
        box.add(icon);
      }
      box.add(label);
      this.add(box);
      this.show_all();
    }
  }
}
