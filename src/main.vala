 /* speedticket
    Copyright (C) 2020  Sebastian Spaeth & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Gtk;
/*using Handy;*/

namespace Speedticket {

[GtkTemplate (ui = "/de/sspaeth/speedticket/ui/window.ui")]
public class ApplicationWindow : Gtk.ApplicationWindow {

  [GtkChild]
  // main Notebook containing all tabs
  private Gtk.Notebook main_tabs;

  [GtkChild]
  // ListBox with PassRow's in the upcoming Tab,
  private Gtk.ListBox ticket_listbox;

  public ApplicationWindow (Gtk.Application application) {
    GLib.Object (application: application);
  }

  public void open (GLib.File file) {
  }

  [GtkCallback]
  public void on_clicked_add_ticket (Button source) {
    debug ("Clicked add ticket\n");
    var dlg = new FileChooserDialog ("Select a ticket", this,
				     FileChooserAction.OPEN,
				     "_Cancel",
                                     ResponseType.CANCEL,
				     "_Open",
                                     ResponseType.ACCEPT,
                                     null);
    var res = dlg.run();

    if (res != ResponseType.ACCEPT) {
      dlg.destroy();
      return;
    }
    string filename = dlg.get_filename();
    dlg.destroy();
    File file = File.new_for_path (filename);
    Pass pass;

    try {
      pass = new Pass.import_file(file);
    } catch (PassError ex) {
      critical("Invalid Pass, not loading %s", ex.message);
      return;
    }
    this.add_pass_to_list( pass );
  }

  [GtkCallback]
  public void on_mainwin_destroy (Gtk.Widget button) {
    debug ("QUIT SPEEDTICKET\n");
    this.get_application().quit ();
  }

  [GtkCallback]
  public void on_show_pass(Gtk.ListBox listbox, Gtk.ListBoxRow passrow) {
  // show a specific pass as a separate stack page
  var row = passrow as PassRow;
  var page = new PassStackPage(row.pass);
  var page_num = this.main_tabs.append_page(page);
  var title = row.pass.get_toplevel_field("organizationName");
  if (title == null) {
      title = "Ticket";
  }
  this.main_tabs.set_tab_label_text (page, title);
  // GTK3: Note that due to historical reasons, GtkNotebook refuses
  //       to switch to a page unless the child widget is visible.
  this.main_tabs.set_current_page(page_num);
  }


  public void add_pass_to_list(Pass pass) {
    /* Add a new 'pass' Entry to our main list of tickets */
    var row = new PassRow(pass);
    this.ticket_listbox.add(row);
  }
}

 public class SpeedticketApp : Gtk.Application {

   private ApplicationWindow window;

   public SpeedticketApp () {
     Object(application_id: "de.sspaeth.speedticket",
	    flags: ApplicationFlags.FLAGS_NONE);
  }


   protected override void activate () {
     this.window = new ApplicationWindow(this);
     this.window.present ();
     GLib.Idle.add(this.load_all_tickets, Priority.HIGH_IDLE);
   }

   public bool load_all_tickets(){
     /* Load all tickets and populate the list
      This is called on application start for the initial population
      return false if the source should be removed, so always return false */
     var datadir_string = Environment.get_user_data_dir() + "/speedticket";
     var datadir = File.new_for_path (datadir_string);
     if (!datadir.query_exists ()) {
       // Do nothing if no datadir exists
       return false;
     }
     try {
       FileEnumerator enumerator = datadir.enumerate_children (
				   "standard::*",
                                   FileQueryInfoFlags.NONE);
       FileInfo info = null;

       while ((info = enumerator.next_file()) != null) {
	 if (info.get_file_type () == FileType.DIRECTORY)
	   continue;
	 File file = File.new_for_path (datadir_string + "/" + info.get_name());
	 Pass pass;
	 
	 try {
	   pass = new Pass.from_file(file);
	 } catch (PassError ex) {
	   critical("Invalid Pass, not loading %s", ex.message);
	   continue;
	 }
	 this.window.add_pass_to_list( pass );
       }
     } catch (Error ex) {
       critical("Could not list file %s", ex.message);
     }
     return false;
   }

   public override void open (GLib.File[] files,
                                   string      hint) {
     /*if (window == null)
       window = new ApplicationWindow (this);
       foreach (var file in files)
       window.open (file);
       window.present ();*/
   }
 }
}

public static int main (string[] args) {
  Speedticket.SpeedticketApp app = new Speedticket.SpeedticketApp ();
  return app.run (args);
}
