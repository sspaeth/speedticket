 /* speedticket
    Copyright (C) 2020  Sebastian Spaeth & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Gtk;
using Zint;
namespace Speedticket {

  public class Barcode {
      public int? type = null; //Symbology from Zint.Barcode. CODE128 is library default
      // The altText attribute of the barcode (or null)
      public string alt_text = null;
      public string msg = null;
      public string encoding = null;

      public Barcode(Json.Node barcode_json) {
          // contains either the "barcode or barcodes node"
          var type = barcode_json.get_node_type();
          Json.Object json_obj = null;
          if (type == Json.NodeType.ARRAY) {
              // TODO, need to handle the barcodes case...
              // For now, simply get the first in a list of barcodes
              json_obj = barcode_json.get_array().get_element(0).get_object();
          } else {
              json_obj = barcode_json.get_object();
          }
          string format = null;
          if (json_obj.has_member("format")) {
              format = json_obj.get_string_member("format");
              }
          if (format == "PKBarcodeFormatAztec") {
              info("Known barcode format AZTEC");
              this.type = Zint.Barcode.AZTEC;}
          else if (format == "PKBarcodeFormatQR") {
              info("Known barcode format QR");
              this.type = Zint.Barcode.QRCODE;}
          else {
              warning("Unknown symology '%s'. PLEASE REPORT", format);
          }
          if (json_obj.has_member("altText"))
              this.alt_text = json_obj.get_string_member("altText");
          if (json_obj.has_member("messageEncoding"))
              this.encoding = json_obj.get_string_member("messageEncoding");
          if (json_obj.has_member("message"))
              this.msg = json_obj.get_string_member("message");
          if (this.encoding == "utf-8") {
              debug("barcode message is in utf-8");
          } else {
              try {
                  this.msg = GLib.convert(this.msg, (ssize_t)this.msg.length,
                                          "utf-8", this.encoding);
              } catch (ConvertError ex) {
                  critical("Failed to convert from barcode message encoding: "+
                           "'%s'.", this.encoding);
                  this.msg = null;
              }
              debug("Convert from barcode message encoding: '%s'.",
                    this.encoding);
          }
          //debug: critical(Json.to_string(barcode_json, true));
      }


      public Gtk.Image? get_image() {
          warning("return an image for %s", this.msg);
          var sym = new Zint.Symbol();
          sym.show_hrt = 0;          // No text below img
          sym.symbology = this.type; // use identified barcode type, QR...
          var err = sym.encode_and_buffer(this.msg);
          if (err != 0) {
              critical("Error Encoding Barcode (%d)", err);
          }

          //Pixbuf.from_data (owned uint8[] data, Colorspace colorspace, bool has_alpha, int bits_per_sample, int width, int height, int rowstride, PixbufDestroyNotify? destroy_fn = free)
          var bitmap = sym.get_bitmap();
          var pbuf = new Gdk.Pixbuf.from_data(bitmap, Gdk.Colorspace.RGB,
                         false, 8, sym.bitmap_width, sym.bitmap_height,
                                              sym.bitmap_width*3);
          var img = new Image.from_pixbuf (pbuf);
          return img;
      }
  }

  [GtkTemplate (ui = "/de/sspaeth/speedticket/ui/pass_details.ui")]
  public class PassStackPage : Gtk.Grid {
    /* the underlying Pass */
    private Pass pass;
    [GtkChild]
    private Gtk.Box logo_box;
    [GtkChild]
    private Gtk.Box primkey_box;
    [GtkChild]
    private Gtk.Box seckey_box;
    [GtkChild]
    private Gtk.Box auxkey_box;
    [GtkChild]
    private Gtk.Box barcode_box;

 /*    0   I    1    I    2    I  3                  I  4  I
    --------------------------------------------------------
0   I logo_box: LOGO  Logotext                             I
    --------------------------------------------------------
1   I Prim I ary     I Values  I ... I
    I Keys I ...     I Values  I ... I
    --------------------------------------------------------
2
    --------------------------------------------------------
3
    --------------------------------------------------------
4
    --------------------------------------------------------
5   I barcode_box: I        BARCODEIMAGE                  II
                   -----------------------------------------
    I              I      BARCODE ALTTEXT                 II 
    --------------------------------------------------------
 */
    /* Constructor opening an empty pass */
    public PassStackPage(Pass pass) {
      debug("Creating pass stack page");
      this.pass = pass;

	
      // Logo in row 0, img in the top left corner
      
      if (pass.logo != null) {
          // Logo goes into top-left (0,0)
            var logo = new Image.from_pixbuf (pass.logo);
            this.logo_box.add(logo);
        } else if (pass.icon != null) {
            // Logo goes into top-left (0,0)
            var icon = new Image.from_pixbuf(pass.icon);
	    this.logo_box.add(icon);
        }


      string? logoText = pass.get_toplevel_field("logoText");
      if (logoText != null) {
	var logotext_lab = new Label(logoText);
	debug("logoText is: %s", logoText);
	this.logo_box.add(logotext_lab);
      }

      //Display Primary Fields
      if (this.pass.primaryFields != null) {
	foreach(var field in this.pass.primaryFields) {
	  if (field == null) break;
	  string s;
	  if (field.label == null) { s = field.key; } else { s = field.key + "\n" + field.label; }
	  s += ": " + field.value;
	  debug("primary field is: %s: %s", s, field.value);
	  var label = new Label(s);
	  this.primkey_box.add(label);
	}
      }

      //Display Secondary Fields
      if (this.pass.secondaryFields != null) {
	foreach(var field in this.pass.secondaryFields) {
	  if (field == null) break;
	  string s;
	  if (field.label == null) { s = field.key; } else { s = field.key + " " + field.label; }
	  s += ": " + field.value;
	  debug("secondary field is: %s: %s", s, field.value);
	  var label = new Label(s);
	  this.seckey_box.add(label);
	}
      }

      //Display auxFields
      if (this.pass.auxFields != null) {
	foreach(var field in this.pass.auxFields) {
	  if (field == null) break;
	  string s;
	  if (field.label == null) { s = field.key; } else { s = field.key + " " + field.label; }
	  s += ": " + field.value;
	  debug("secondary field is: %s: %s", s, field.value);
	  var label = new Label(s);
	  this.auxkey_box.add(label);
	}
      }

      //get the pass.json object
      var object = pass.pass.get_object();
      Json.Node barcode_node = null;
      Barcode barcode = null;
      if (object.has_member("barcode"))
	barcode_node = object.get_member("barcode");
      if (object.has_member("barcodes"))
	barcode_node = object.get_member("barcodes");
      if (barcode_node != null) {
	debug("Adding barcode");
	barcode = new Barcode(barcode_node);
	var barcode_img = barcode.get_image();
	this.barcode_box.add(barcode_img);
	var alt_text = barcode.alt_text;
	if (alt_text != null) {
	  var barcodetext_lab = new Label(alt_text);
	  debug("Barcode Text is: %s", alt_text);
	  this.barcode_box.add(barcodetext_lab);
	}
      }
      this.show_all();
    }
  }
}
