 /* speedticket
    Copyright (C) 2020  Sebastian Spaeth & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* see https://developer.apple.com/library/archive/documentation/UserExperience/Reference/PassKit_Bundle/Chapters/PackageStructure.html
https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/PassKit_PG/Creating.html

background.png: The image displayed as the background of the front of the pass.
footer.png: The image displayed on the front of the pass near the barcode.
icon.png: The pass’s icon. This is displayed in notifications and in emails that have a pass attached, and on the lock screen.
logo.png: The image displayed on the front of the pass in the top left.
manifest.json: A JSON dictionary. Each key is the path to a file (relative to the top level of the bundle) and the key’s value is the SHA-1 hash for that file. Every file in the bundle appears in the manifest, except for the manifest itself and the signature.
pass.json: A JSON dictionary that defines the pass.
signature: A detached PKCS #7 signature of the manifest.json file.
strip.png: The image displayed behind the primary fields on the front of the pass.
thumbnail.png:     An additional image displayed on the front of the pass. For example, on a membership card, the thumbnail could be used to a picture of the cardholder.
Note: All of the pass’s images are loaded using standard UIImage image-loading methods. This means, for example, the file name of the high-resolution version of the image ends with @2x.png.

Standard Keys - Information that is required for all passes
--------------------------------------------------------------
description: localizable string Required. lets VoiceOver make your pass accessible to blind and low-vision users. Don’t try to include all of the data on the pass in its description, just include enough detail to distinguish passes of the same type.

organizationName: localizable string. Display name of the organization that originated and signed the pass.

passTypeIdentifier: string. Pass type identifier, as issued by Apple. The value must correspond with your signing certificate.

serialNumber: string. Serial number that uniquely identifies the pass. No two passes with the same pass type identifier may have the same serial number.

teamIdentifier: string. Team identifier of the organization that originated and signed the pass, as issued by Apple.

appLaunchURL: string Optional. A URL to be passed to the associated app when launching it.

expirationDate: W3C date, as a string Optional. Date and time when the pass expires.

voided: Boolean Optional. Indicates that the pass is void—for example, a one time use coupon that has been redeemed. The default value is false.

relevantDate: W3C date, as a string Recommended for event tickets and boarding passes; otherwise optional. Date and time when the pass becomes relevant.

TODO: Style Keys, Visual Appearance Keys, Web Service Keys,NFC-Enabled Pass Keys
*/
using Gee;
using Gtk;
using Json;

namespace Speedticket {

public errordomain PassError {
    MissingData,
    InvalidData,
    Error
}

public enum PassType {
    GENERIC,     //generic,
    BOARDINGPASS,//boardingPass
    EVENTTICKET, //eventTicket
    COUPON,      //coupon
    STORECARD,   //storeCard
    UNKNOWN      //any other
}


public class Pass {

  // Public attributes:
  // corresponding ticket file in our datadirectory
  public File file = null;

  // icon.png.  Use new Image.from_pixbuf (this.icon) to get a Gtk.Image
  public Gdk.Pixbuf icon = null;
  // logo.png. Use new Image.from_pixbuf (this.logo) to get a Gtk.Image
  public Gdk.Pixbuf logo = null;
  // thumbnail.png. Use new Image.from_pixbuf (this.thumbnail) for a Gtk.Image
  public Gdk.Pixbuf thumbnail = null;
  // one of PassType
  public PassType type = PassType.UNKNOWN;
  // Primary Fields
  public Gee.LinkedList<PassField?> primaryFields = null;
  // Secondary Fields
  public Gee.LinkedList<PassField?> secondaryFields = null;
  // Header Fields
  public Gee.LinkedList<PassField?> headerFields = null;
  // Auxiliary Fields
  public Gee.LinkedList<PassField?> auxFields = null;

  /* filename - filecontent mapping */
  private TreeMap<string, void> files;
  /* filename - filehash mapping */
  private HashMap<string, string> files_hashes;
  /* detached PKCS #7 signature of manifest.json */
  /* TODO: verify signature! */
  private Bytes? signature = null;
  /* unique string, combination of pass type identifier and serialNumber
     is used to uniquely identify a pass. */
  public string serialNumber = null;
  public string passTypeIdentifier = null;
  /* pass.json */
  //TODO: make private and provide enough getter functions...
  public Json.Node pass;

  /* Constructor opening an empty pass */
  public Pass() {
    debug("Creating pass");
    this.files = new TreeMap<string, void> ();
    this.files_hashes = new HashMap<string, string> ();
    this.primaryFields = null;
    this.secondaryFields = null;
    this.headerFields = null;
    this.auxFields = null;
  }

  public Pass.import_file(File passfile) throws PassError {
    /* Import pkpass and store in app */
    debug("Importing pass");

    /* copy file to internal storage and open internal version
       First, make sure the data directory exists.  */
    var datadir_string = Environment.get_user_data_dir() + "/speedticket";
    var datadir = File.new_for_path (datadir_string);
    debug("Checking existence of %s", datadir.get_path());
    if (!datadir.query_exists ()) {
        try {
          var success = datadir.make_directory();
          if (!success) error("Failed to create data directory %s",
                              datadir_string);
        } catch (Error ex) {
            throw new PassError.Error(ex.message);
        }
    }
    debug("Datadir exists.");
    // Next, make a copy of the file in our internal datadir (renaming it .zip)
    // Use a random filename, making sure it does not exist yet.
    File destination = null;
    do {
      var rand_name = Uuid.string_random () + ".zip";
      destination = File.new_for_path (datadir_string + "/" + rand_name);
      debug("Suggesting %s for imported file", destination.get_path());
    } while (destination.query_exists());
    try {
      passfile.copy (destination, FileCopyFlags.TARGET_DEFAULT_PERMS);
    } catch (Error ex) {
      // TODO: Need to handle and report error, informing the user about a
      //       failure.
      critical("Error importing ticket: %s", ex.message);
      return;
    }

    // Finally open the imported file as we would do with any other file
    this.from_file(destination);
  }

  public Pass.from_file(File file) throws PassError {
    /* open pkpass from internal storage */
    this();
    this.file = file;
    this.read_file();
    this.sanity_check();
    this.populate_fields();
  }

  private LinkedList<Speedticket.PassField?>? extract_PassFields(Json.Node node) {
    /* extract a PassField (Standard Field Dictionary Keys) from a Json.Node
       returns null on defect nodes */
    if (node.is_null()) {
      warning("Primary Fields Node is a NULL node!");
      return null;
    }
    if (node.get_node_type() != Json.NodeType.ARRAY) {
      warning ("Standard Field Dictionary Node is NOT AN ARRAY!");
      return null;
    }
    var fields_array = node.get_array();	
    var fields = new LinkedList<PassField?>();
    uint len = fields_array.get_length();
    for (uint i=0; i<len; ++i) {
      var entry = fields_array.get_element(i).get_object();
      var field = PassField();
      //debug(Json.to_string(fields_array.get_element(i), true));
      field.key = entry.get_string_member_with_default("key","");
      field.value = entry.get_string_member_with_default("value","");
      if (entry.has_member("label")) {
	field.label = entry.get_string_member("label");
      } else {
	field.label = null;
      }
      if (entry.has_member("changeMessage")) {
	field.changeMessage = entry.get_string_member("changeMessage");
      } else {
	field.changeMessage = null;
      }
      fields.add(field);
    }
    return fields;
  }
  
  private void populate_fields() {
      /* Populate the fields, needs to be run after sanity_check() */
      Json.Object? object = this.pass.get_object();
      assert(object != null);
      //dictionary containing the keys in Pass Structure Dictionary Keys
      Json.Object? pass_structure = null;
      // check and store the PassType
      if (object.has_member("boardingPass")) {
	pass_structure = object.get_object_member("boardingPass");
	this.type = PassType.BOARDINGPASS;
      } else if (object.has_member("eventTicket")) {
	pass_structure = object.get_object_member("eventTicket");
	this.type = PassType.EVENTTICKET;
      } else if (object.has_member("generic")) {
	pass_structure = object.get_object_member("generic");
	this.type = PassType.GENERIC;
      } else if (object.has_member("coupon")) {
	pass_structure = object.get_object_member("coupon");
	this.type = PassType.COUPON;
      } else if (object.has_member("storeCard")) {
	pass_structure = object.get_object_member("storeCard");
	this.type = PassType.STORECARD;
      } else {
	warning("Unknown Ticket type. Please report!");
	pass_structure = null;
	this.type = PassType.UNKNOWN;
      }

      // Nothing to do for unknown passes
      if (pass_structure == null) {
	return;
      }
      if (pass_structure.has_member("primaryFields")) {
	//populate primaryFields
	var fields_node = pass_structure.get_member("primaryFields");
	this.primaryFields = this.extract_PassFields(fields_node);
      }
      if (pass_structure.has_member("secondaryFields")) {
	//populate secondaryFields
	var fields_node = pass_structure.get_member("secondaryFields");
	this.secondaryFields = this.extract_PassFields(fields_node);
      }
      if (pass_structure.has_member("headerFields")) {
	//populate headerFields
	var fields_node = pass_structure.get_member("headerFields");
	this.headerFields = this.extract_PassFields(fields_node);
      }
      if (pass_structure.has_member("auxFields")) {
	//populate auxFields
	var fields_node = pass_structure.get_member("auxFields");
	this.auxFields = this.extract_PassFields(fields_node);
      }
  }

  public string get_description() {
    /* Returns the mandatory 'description' field */
    var object = this.pass.get_object();
    if (!object.has_member("description")) {
      warning("Required attribute 'description' is missing.");
      return "";
    }
    return object.get_member("description").get_string();
  }

  public string? get_background_color() {
    /* Returns the mandatory 'description' field */
    var object = this.pass.get_object();
    if (!object.has_member("backgroundColor"))
      return null;
    return object.get_member("backgroundColor").get_string();
  }

  public string? get_toplevel_field(string str) {
    /* Returns a top-level field from pass.json as string or null */
    var object = this.pass.get_object();
    if (!object.has_member(str)) {
      info("get_toplevel_field %s returns null", str);
      return null;
    }
    info("get_toplevel_field %s returns %s", str, object.get_member("str").get_string());
    return object.get_member(str).get_string();
  }


  private void sanity_check() throws PassError{
    /* Checks whether the pass data is a valid pass */

    if (this.pass == null) {
      throw new PassError.MissingData(
        "Pass object has not yet read valid data.");
    }

    var object = this.pass.get_object();

    /* formatVersion: Version of the file format. The value must be 1.*/
    if (object.has_member("formatVersion")) {
      var version = object.get_int_member_with_default("formatVersion", 1);
      if (version != 1) {
        throw new PassError.InvalidData(
            "formatVersion must be 1 but is %d".printf((int)version));
      }
    } else {
      warning("Required attribute 'formatVersion' is missing.");
    }

    //store passTypeIdentifier
    if (object.has_member("passTypeIdentifier")) {
        var identifier = object.get_string_member("passTypeIdentifier");
        if (identifier.substring(0, 5) != "pass.") {
            warning("'passTypeIdentifier' does not start with 'pass.'");
        }
        this.passTypeIdentifier = identifier;
    } else {
        warning("Required attribute 'passTypeIdentifier' is missing.");
    }

    if (this.passTypeIdentifier == null) {
        warning("PKPASS does not contain a 'passTypeIdentifier'");
    }

    //store serialNumber
    if (object.has_member("serialNumber")) {
        var serial = object.get_string_member("serialNumber");
        this.serialNumber = serial;
    }
    if (this.signature == null) {
        warning("PKPASS does not contain a 'signature'");
    }
  }

  private Bytes archive_read_entry(Archive.Read archive) {
    /* Read the current archive.Entry return the Bytes() */
    var data = new ByteArray();
    ssize_t size = 1;
    while (size > 0) {
      var buffer = new uint8[4096];
      size = archive.read_data(buffer);
      data.append(buffer);
      /* TODO: size < 0 on error?! */
    }
    return ByteArray.free_to_bytes(data);
  }

  private Json.Node archive_read_json_entry(Archive.Read archive) {
    /* Read the current archive.Entry and hand back the root Json.Node */
    Json.Parser parser = new Json.Parser ();
    var bytes = this.archive_read_entry(archive);

    try {
      parser.load_from_data ( (string) bytes.get_data() );
    } catch (Error e) {
      stderr.printf ("Error parson manifest json");
    }
    return parser.get_root ();
  }

  public void read_file() {
    /* Read in the zip archive */
    Archive.Read archive = new Archive.Read ();
    archive.support_format_zip ();

    var res = archive.open_filename(this.file.get_path(), 10240);
    if (res != Archive.Result.OK) {
      critical ("Error opening %s: %s (%d)", file.get_path(),
                archive.error_string (), archive.errno ());
      /* TODO: proper error handling*/
      return;
    }

    /* Iterate headers, store files, file_hashes from manifest.json and
       signature */
    unowned Archive.Entry entry;
    Archive.Result last_result;
    while ((last_result = archive.next_header (out entry)) == Archive.Result.OK) {

      if (entry.pathname() == "manifest.json") {
          /* Read in manifest json and store internally*/
          info ("READ THE MANIFEST!");
          var node = this.archive_read_json_entry(archive);
          var object = node.get_object();
          foreach (string key in object.get_members ()) {
              var hash = object.get_string_member(key);
              this.files_hashes.set (key, hash);
          }
      } else if (entry.pathname() == "signature") {
          /* Read in manifest json signature and store internally*/
          this.signature = this.archive_read_entry (archive);
      } else if (entry.pathname() == "pass.json") {
          /* Read in pass.json and store internally*/
          debug ("reading pass.json");
          this.pass = this.archive_read_json_entry(archive);
          debug(Json.to_string(this.pass, true));
      } else if (entry.pathname() == "logo.png") {
          info("Reading logo.png");
          var bytes = this.archive_read_entry (archive);
          var stream = new MemoryInputStream.from_bytes (bytes);
          try {
              //throws Error
              this.logo = new Gdk.Pixbuf.from_stream(stream, null);
          } catch (Error ex) {
              warning("Could not read logo.png: %s", ex.message);
              break;
          }
      } else if (entry.pathname() == "icon.png") {
          debug("Reading icon.png");
        var bytes = this.archive_read_entry (archive);
        var stream = new MemoryInputStream.from_bytes (bytes);
        try {
            //throws Error
            this.icon = new Gdk.Pixbuf.from_stream(stream, null);
        } catch (Error ex) {
            warning("Could not read logo.png: %s", ex.message);
            break;
        }
      } else if (entry.pathname() == "thumbnail.png") {
          info("Reading thumbnail.png");
          var bytes = this.archive_read_entry (archive);
          var stream = new MemoryInputStream.from_bytes (bytes);
          try {
              //throws Error
              this.thumbnail = new Gdk.Pixbuf.from_stream(stream, null);
          } catch (Error ex) {
              warning("Could not read thumbnail.png: %s", ex.message);
              break;
          }
      } else {
          stderr.printf ("Unhandled file: %s\n", entry.pathname());
      }
    }
    //foreach (var e in this.files_hashes.entries)
    //    debug("Found file in pkpass: %s, Hash %s", e.key, e.value);

    }


/*
var home_dir = File.new_for_path (Environment.get_home_dir ());
  var data_file = File.new_for_path ("data.txt");
  if (file.query_exists ()) {
    // File or directory exists
  }
  if (file.query_file_type (0) == FileType.DIRECTORY) {
    // It's a directory
  }

  // Make a copy of file
        var destination = File.new_for_path ("samplefile.bak");
        file.copy (destination, FileCopyFlags.NONE);

  // Rename file
        var renamed = file.set_display_name ("samplefile.data");

        // Delete copy
        destination.delete ();

*/

}
}
