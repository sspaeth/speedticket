# speedticket #

Store and display tickets quickly on a (Linux) mobile phone. It is
targeted to the Librem 5 and the Pinephone running mobian.

It imports and displays .pkpass files (and .espass files) as used by
Apple Wallet, Passandroid and other programs.

Its canonical home is at: https://gitlab.com/sspaeth/speedticket

## Build from source ##

### Prerequisites for Debian ###
build requirements: valac meson libgtk-3-dev libgee-0.8-dev libjson-glib-dev libarchive-dev cmake (cmake-vala?) libzint-dev

Runtime requirements: libzint2.9

### Building ###

  meson . build
  ninja -C build install

## Debugging ##

All debug messages are being displayed when invoked as:

  G_MESSAGES_DEBUG=all speedticket

## License ##

Copyright (C) 2020-($CUR_YEAR)  Sebastian Spaeth & contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
